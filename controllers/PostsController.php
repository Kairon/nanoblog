<?php

namespace app\controllers;

use Yii;
use app\models\Posts;
use app\models\search\PostSearch;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

class PostsController extends Controller
{
    //поведение
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' =>[
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Список всех постов
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        if(!Yii::$app->user->identity->getIsAdmin()) {
            $searchModel->user_id = Yii::$app->user->id;
        };
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //Отображает одну запись
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    //Создание новой записи
    public function actionCreate()
    {

        $model = new Posts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->save($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    private function save($model)
    {
        if($model->imageFile = UploadedFile::getInstance($model,'imageFile')){
            if($model->logo){
                @unlink($model->logo);
            }
            $imageName = 'uploads/'.$model->title . '.' . $model->imageFile->extension;
            $model->imageFile->saveAs($imageName );
            $model->logo = $imageName;
            $model->save(false);
        }
    }

    //Редактирование записи.
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->save($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    //Удаление записей
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    //Находит модель записи на основе ее значения первичного ключа.
    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            //запрет доступа для пользователей
            if($model->user_id !=Yii::$app->user->id && !Yii::$app->user->identity->getIsAdmin()){
                throw new ForbiddenHttpException(Yii::t('app','Access denied'));
            }
            //запрет доступа для пользователей---
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
