<?php

namespace app\controllers;

use app\models\Portfolios;
use app\models\Posts;
use app\models\RegForm;
use app\models\search\PostSearch;
use app\models\LoginForm;
use app\models\ContactForm;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    //поведение
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    //
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    //отображение главной страници
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Posts::find()->where(['private' => '0']),
            'pagination' => [
                'defaultPageSize' => 5,
                'forcePageParam' => false,
                'pageSizeParam' => false,
            ],
            'sort' => [
                'defaultOrder'=>[
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    //страница записи со  слагом
    public function actionView( $slug )
    {
        if(false != ($model = Posts::findOne([
                'slug' => $slug,
                'private' => '0',
            ]))){
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            throw new NotFoundHttpException(Yii::t('app','Post not found'));
        }
    }

    //страница портфолио
    public function actionPortfolios()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Portfolios::find(),
            'pagination' => [
                'defaultPageSize' => 5,
                'forcePageParam' => false,
                'pageSizeParam' => false,
            ],
        ]);

        return $this->render('portfolios', [
                'dataProvider' => $dataProvider

        ]);
    }

    //вывод тегов
    public function actionTag()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, '');


        return $this->render('index',[
            'searchModel'=>$searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //вывод категорий
    public function actionCategory()
    {
        return $this->actionTag();
    }

    //страница авторизации
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'login';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    //страница регистрации
    public function actionRegister()
    {
        $this->layout = 'reg';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegForm();

        $this->performAjaxValidation($model);


        if ($model->load(Yii::$app->request->post()) && $model->reg()) {
            return $this->goBack();
        }

        return $this->render('register', [
            'model' => $model ,
        ]);
    }

    //logout
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

   //страница контактов
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    //страница about
    public function actionAbout()
    {
        return $this->render('about');
    }

    protected function performAjaxValidation($model)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data   = ActiveForm::validate($model);
            Yii::$app->response->send();
            Yii::$app->end();
        }
    }


}
