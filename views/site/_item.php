<?php

use yii\helpers\Html;

Yii::$app->request->url;

?>

<article class = "blog-wrap">

    <header class="page-header blog-title">
        <div class="author-wrap"> <span class="inside"> <a href="#"><img class="img-responsive" alt="" src="images/post-image/02_team.png"></a> </span> </div>
        <h4><?= $model->user->login ?></h4>
        <div class="post-meta">
            <ul class="list-inline">
                <li><span class="fa fa-user"></span><a href="/"><?= $model->user->login ?> /</a></li>
                <li><span class="fa fa-calendar"></span><a href="/"><?= Yii::$app->formatter->format($model->date ,'date') ?> /</a></li>
                <li><span class="fa fa-code-fork"></span><a href="/"><?= $model->category->name?></a></li>
            </ul>
        </div>
    </header>
    <div class="post-desc">
        <h3><?= $model->title ?></h3>
        <?php if($model->getLogo()) : ?>
        <div class="blog-media"> <img alt="" src="<?= $model->getLogo() ?>"> </div>
        <?php endif;?>
        <p><?= $model->getShort()?> </p>
        <span class="fa fa-category"></span>
        <div id="sidebar">
        <div class="widget">
            <div class="tagcloud"> <?= $model->showTags()?></div>
        </div>
        </div>

        <?= Html::a(Yii::t('app','Detail'),['view','slug' => $model->slug,], ['class' =>'btn btn-default'])?>
    </div>

</article>
