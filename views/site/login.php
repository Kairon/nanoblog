<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('app','Log in');
$this->params['breadcrumbs'][] = $this->title;
?>

<!----------------------------------------------------------------->

<?php $form = ActiveForm::begin([
]); ?>

<aside class="col-xs-12 textbox">
    <h2 class="black"><?=Yii::t('app','Log in');?></h2>
    <?php $form = ActiveForm::begin(['id' => 'login-form']);?>
    <?=$form->field($model, 'username',
        [
            'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-user'></i></span>{input}</div>\n{hint}\n{error}"
        ])
        ->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Username')])->label(false); ?>

        <div class="form-group">
            <?=$form->field($model, 'password',
                [
                    'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-key'></i></span>{input}</div>\n{hint}\n{error}"
                ])
                ->passwordInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Password')])->label(false); ?>

        </div>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox">
                    Remember me</label>
                <?= Html::a(Yii::t('app','Registration'),['/site/register'])?> </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app','Log in2'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
           <!-- <button class="dmbutton btn btn-primary" type="submit">Login</button>-->
        </div>
    </form>
    <!--textbox-->
</aside>
<?php ActiveForm::end(); ?>