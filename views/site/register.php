<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app','Registration');
$this->params['breadcrumbs'][] = $this->title;
?>

<!------------------------------------------>
<?php $form = ActiveForm::begin([
]); ?>

    <aside class="col-xs-12 textbox">
        <h2 class="black"><?= Yii::t('app','Registration')?></h2>
        <?php $form = ActiveForm::begin([
            //'id' => $model->formName(),
            'enableAjaxValidation' => true,
            'enableClientValidation' =>false,
        ]);?>
        <?=$form->field($model, 'username',
            [
                'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-user'></i></span>{input}</div>\n{hint}\n{error}"
            ])
            ->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Username')])->label(false); ?>

        <div class="form-group">

            <!---->
            <?=$form->field($model, 'pass',
                [
                    'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-key'></i></span>{input}</div>\n{hint}\n{error}"
                ])
                ->passwordInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Password')])->label(false); ?>

            <!---->
            <?=$form->field($model, 'passRepeat',
                            [
                                'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-repeat'></i></span>{input}</div>\n{hint}\n{error}"
                            ])
                            ->passwordInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Pass Repeat')])->label(false); ?>

            <!---->
            <?=$form->field($model, 'email',
                                        [
                                            'template' => "<div class='input-group'><span class='input-group-addon'><i class='fa fa-envelope' aria-hidden=\"true\"></i>
                                            </span>{input}</div>\n{hint}\n{error}"
                                        ])
                                        ->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Mail')])->label(false); ?>
            <!---->


        </div>

        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-5">{image}</div><div class="col-lg-7">{input}</div></div>',
            'options' => [
                'class' => 'form-control',
                'placeholder' => Yii::t('app','Captcha'),
            ],
        ])->label(false) ?>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <?= Yii::t('app','Isset login')?></label>
                <?= Html::a(Yii::t('app','Log in2'),['/site/login'])?> </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app','Btn reg'), ['class' => 'btn btn-primary']) ?>
            <!-- <button class="dmbutton btn btn-primary" type="submit">Login</button>-->
        </div>
        </form>
        <!--textbox-->
    </aside>
<?php ActiveForm::end(); ?>