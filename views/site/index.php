<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Categories;
use app\widgets\Categories as CategoriesWidget;


?>



<!-- BLOG STARTS
========================================================================= -->

        <div class="row header">

<div id="sidebar" class="col-sm-3 col-xs-12">
    <div class="search-bar">
        <form method="get">
            <fieldset>
                <input type="text" value="Search on this site..." class="search_text showtextback">
                <button type="submit" aria-expanded="false" class="pull-right"></button>
            </fieldset>
        </form>
    </div>
    <?= CategoriesWidget::widget([
        'items' => Categories::find()->all(),
    ])?>
    <!-- end widget -->
    <div class="widget">
        <h4 class="title"> <span><?= Yii::t('app','Tags')?></span> </h4>
        <div class="tagcloud"> <?= \app\models\Tags::getAll() ?></div>
    </div>
</div>
<!-- end sidebar -->
<div class="pull-right col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix">
                <?php Pjax::begin() ?>

                    <?= ListView::widget([
                        'dataProvider'=>$dataProvider ,
                        'itemView' => '_item',

                        'layout' => "{items}",
                    ]) ?>

                <div class="clearfix"></div>
                <hr>

                <div class=" text-center">
                   <?= \yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination])?>
                </div>
                <?php Pjax::end() ?>
</div>



        </div>
    </div>
</div>