<?php
$this->title = $model->title;
$this->params['keywords'] = $model->key_words;
$this->params['description'] = $model->description;
?>
<?php if($model->getLogo()) : ?>
    <div class="blog-media"> <img alt="" src="<?= $model->getLogo() ?>"> </div>
<?php endif;?>
<h1><?= $model->title ?></h1>
<p>
<div class="post-meta">
    <ul class="list-inline">
        <li><span class="fa fa-user"></span><a href=""><?= $model->user->login ?> /</a></li>
        <li><span class="fa fa-calendar"></span><a href=""><?= Yii::$app->formatter->format($model->date ,'date') ?> /</a></li>
        <li><span class="fa fa-code-fork"></span><a href=""><?= $model->category->name?></a></li>
    </ul>
</div>
</p>

<p><?= $model->full_text ?></p>
<p><?php//echo Html::a(Yii::t('app', 'Back to search results'), $backUrl) ?></p>