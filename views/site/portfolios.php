<?php

$this->title = 'Portfolio';

use yii\widgets\ListView;
use app\models\Portfolios;


?>
<?php $this->beginBlock('block1') ?>
<aside class="col-xs-12 subbanner" xmlns="http://www.w3.org/1999/html">
    <h1><?=$this->title?></h1>
    <p>Вы посетили блог Богдана Ивахнова , приятного просмотра</p>
</aside>
<div class="clearfix"></div>
<?php $this->endBlock() ?>
<?php
    //Подключение дополнительных Js для работы сортировки
    Yii::$app->view->registerJsFile('@web/js/masonary/masonry3.1.4.js', ['depends' => 'yii\web\YiiAsset']);
    Yii::$app->view->registerJsFile('@web/js/masonary/masonry.filter.js', ['depends' => 'yii\web\YiiAsset']);
?>

<div class="site-index">
    <div class="body-content">
        <div class="container padding-box" id="portfolio">
            <div class="row header">
                <article class="col-xs-12 textbox text-center">
                    <h2 class="black">Our Portfolio</h2>
                    <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                    <div class="portfolio-section col-xs-12">
                        <ul id="cartegories" class="list-inline">
                            <li><a data-filter="" class="filter active">All</a></li>
                            <?php foreach (Portfolios::find()->select('tag')->distinct()->asArray()->all() as $tag) : ?>
                                <li><a data-filter="<?=$tag['tag']?>" class="filter"><?=$tag['tag']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="col-xs-12" id="porfolio-masonry">
                            <?= ListView::widget([
                                'dataProvider'=>$dataProvider ,
                                'itemView' => '_item2',
                                'layout' => "{items}",
                            ]) ?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
