<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Categories;
use app\models\Tags;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">

    <?php $form = ActiveForm::begin( ['options' => ['enctype' =>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_text')->widget(CKEditor::className(),[
            'options' => ['row '=> 6],
            'preset' => 'full',
        ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'key_words')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'private')->checkbox(['value' =>'1','uncheck' => '0'])?>

    <?= $form->field($model, 'tags')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Tags::find()->all(), 'id', 'name'),
        'options'=>[
            'placeholder' => 'Select a state ...',
            'multiple' => true ,
        ],

    ]) ?>

    <?= $form->field($model, 'category_id')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
        'options'=> [
            'placeholder' => 'Select a state ...',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
