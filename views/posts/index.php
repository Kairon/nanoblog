<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Categories;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Posts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            //'full_text:ntext',
           // 'description:ntext',
            //'key_words:ntext',
            [
                'attribute' => 'logo',
                'format' => 'raw',
                'value' => function($model){
                return Html::img($model->getLogo(),['class' => 'img-responsive', 'style' => 'max-width : 150px']);
            }
            ],
             'date:date',
            'user.login',
            // 'private',
            // 'slug',
             [
                 'attribute' => 'category_id',
                 'value' => 'category.name',
                 'filter' => Select2::widget([
                     'model' => $searchModel,
                     'attribute' => 'category_id',
                     'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
                     'options' => [
                         'placeholder' => Yii::t('app','Choose category'),
                         'allowClear' => true,
                     ],
                 ])
             ] ,

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
