<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Html::img('/images/logo.png'),
//    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'encodeLabels' => false,
        'class' => 'navbar-inverse affix-top',
        'tag' => 'div'
    ],
]);

//Создание массива
$items2 = [];
$items = [
    ['label' => Yii::t('app', 'Home'), 'url' => ['/']]
];
if(Yii::$app->user->isGuest){
    $items[] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/site/portfolios']];
    $items2[] = ['label' => Yii::t('app','Log in'),'url' => ['/site/login']];
    $items2[] = ['label' => Yii::t('app', 'Registration'),'url' => ['/site/register']];
}else{
    if(Yii::$app->user->identity->getIsAdmin()) {
        $items[] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/site/portfolios']];
        $items2[] = ['label' => Yii::t('app','Settings'),'items'=>[
            ['label' => Yii::t('app', 'Posts'), 'url' => ['/posts/index']],
            ['label' => Yii::t('app', 'Categories'), 'url' => ['/categories/index']],
            ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/portfolios/index']],
            ['label' => Yii::t('app', 'Tags'), 'url' => ['/tags/index']],

        ]];


    }
    if(!Yii::$app->user->identity->getIsAdmin() && !Yii::$app->user->isGuest){
        $items[] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/site/portfolios']];
        $items2[] = ['label' => Yii::t('app','settings'),'items'=>[
            ['label' => Yii::t('app', 'Posts'), 'url' => ['/posts/index']],
        ]];
    }
    $items2[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];
}
//создание массива---

echo Nav::widget([
    'options' => ['class' => 'navbar-nav affix-top'],
    'items' => $items,
]);

echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav nav_top_rt'],
    'items' => $items2
]);

NavBar::end();
?>