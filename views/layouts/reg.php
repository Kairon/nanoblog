<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;


AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!doctype html>
    <html lang="<?=Yii::$app->language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?=Yii::$app->charset; ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <?php $this->beginBody() ?>
    <body class="login" id="page-top">
    <!-- LOGIN START
    ========================================================================= -->
    <div class="blue-color full-container text-center">
        <div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 login-box"> <a class="logo" href="/"><img alt="logo" src="images/logo.png"></a>
                <?= $content ?>
                <div class="copyright-text"><?=Yii::t('app','My company');?> <?= date('Y') ?></div>
                <!--login-box-->
            </div>
        </div>
        <!--blue-color-->
    </div>
    <!-- /. LOGIN ENDS
        ========================================================================= -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    </body>
    <?=$this->endBody(); ?>
    </html>
<?php $this->endPage() ?>