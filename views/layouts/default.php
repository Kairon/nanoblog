<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    //Создание массива
    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/']]
    ];
    if(Yii::$app->user->isGuest){
        $items[] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/site/portfolios']];
        $items[] = ['label' => Yii::t('app','Login'),'url' => ['/site/login']];
        $items[] = ['label' => Yii::t('app', 'Registration'),'url' => ['/site/register']];
    }else{
        if(Yii::$app->user->identity->getIsAdmin()) {
            $items[] = ['label' => Yii::t('app','settings'),'items'=>[
                ['label' => Yii::t('app', 'Posts'), 'url' => ['/posts/index']],
                ['label' => Yii::t('app', 'Categories'), 'url' => ['/categories/index']],
                ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/portfolios/index']],
                ['label' => Yii::t('app', 'Tags'), 'url' => ['/tags/index']],

            ]];


        }
        if(!Yii::$app->user->identity->getIsAdmin() && !Yii::$app->user->isGuest){
            $items[] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['/site/portfolios']];
            $items[] = ['label' => Yii::t('app','settings'),'items'=>[
                ['label' => Yii::t('app', 'Posts'), 'url' => ['/posts/index']],
            ]];
        }
        $items[] = ['label' => Yii::t('app', 'LLogout'), 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];
    }
    //создание массива---

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
