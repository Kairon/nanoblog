<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

$js = "
    // FIXED Menu
    jQuery('.navbar').affix({
        offset: {
            top: 1
        }
    });
    
    // tooltip demo
    $(\"[data-toggle=tooltip]\").tooltip();
    
     // popover demo
    $(\"[data-toggle=popover]\")
        .popover()
    ";
$this->registerJs($js,\yii\web\View::POS_READY);

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!doctype html>
<html lang="<?=Yii::$app->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=Yii::$app->charset; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
</head>
<body id="page-top">

<?php $this->beginBody() ?>



<div id="loader"></div>

<div id="section-home">
    <header id="header">
        <!-- NAVIGATION STARTS
    ========================================================================= -->
        <nav id="navigation">
            <?=$this->render('_navbar'); ?>
            <div class="navspacer"></div>
        </nav>
        <!-- /. NAVIGATION ENDS
    ========================================================================= -->
    </header>
    <?php if(isset($this->blocks['block1'])): ?>
        <?= $this->blocks['block1']?>
    <?php endif;?>
</div>

<div class="padding-box">
    <div class="container">

                <?= $content ?>
        </div>
    </div>

<div class="padding-box blue-color">
    <div class="container">
        <div class="row header">
            <article class="col-xs-12 social-icons text-center">
                <h2 class="white">Join The 115,000+ Satisfied Users!</h2>
                <ul class="list-inline">
                    <li><a href="#"><span class="fa fa-facebook"></span>facebook</a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span>Twitter</a></li>
                    <li><a href="#"><span class="fa fa-google-plus"></span>Google+</a></li>
                    <li><a href="#"><span class="fa fa-dribbble"></span>Dribble</a></li>
                    <li><a href="#"><span class="fa fa-linkedin"></span>Linkedin</a></li>
                    <li><a href="#"><span class="fa fa-instagram"></span>Instagram</a></li>
                </ul>
                <!--textbox-->
            </article>
            <!--row-->
        </div>
        <!--container-->
    </div>
</div>
<div class="padding-box black2-color">
    <div class="container">
        <div class="row header">
            <aside class="col-sm-4 col-xs-12 small_box"><span class="fa fa-envelope"></span><a href="mailto:hello@designingmedia.com">hello@designingmedia.com</a></aside>
            <aside class="col-sm-5 col-xs-12 small_box"><span class="fa fa-home"></span>121, King Street, Melbourne Victoria 3000</aside>
            <aside class="col-sm-3 col-xs-12 small_box"><span class="fa fa-phone-square"></span>(0800) 234 56789</aside>
            <!--row-->
        </div>
        <!--container-->
    </div>
</div>
<!-- SOCIAL SECTION END

========================================================================= -->
<a class="dmtop" href="#page-top" style="bottom: 25px;"></a>
<!-- FOOTER STARTS
========================================================================= -->

<!-- FOOTER END -->
<?=$this->endBody(); ?>
</body>
</html>
<?php $this->endPage() ?>