<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use leandrogehlen\treegrid\TreeGrid;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$js = "
$('table').css('opacity', 0.3);
$.each($('tr'), function(){
    var tr = $(this);
    var tree_id = tr.attr('class');
    if(tree_id){tree_id = tree_id.split(' ')[0].replace('treegrid-', '');
        var data_key = tr.attr('data-key');
    }
    tr.html(tr.html()
        .replace(data_key, tree_id)
        .replace(data_key, tree_id)
        .replace(data_key, tree_id));
});

$('table').css('opacity', 1);

";

$this->registerJs($js, yii\web\View::POS_END);
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id', //
        'parentColumnName' => 'parent_id',//
        'parentRootValue' => '0', //first parentId value
        //'filterModel' => $searchModel,
        'pluginOptions' => [
            'initialState' => 'collapsed',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',
            'key_words:ntext',
            'parent_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>