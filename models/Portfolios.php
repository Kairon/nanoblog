<?php

namespace app\models;

use yii\web\UploadedFile;
use Yii;


class Portfolios extends \yii\db\ActiveRecord
{
    public $img;

    public static function tableName()
    {
        return 'portfolios';
    }

    public function rules()
    {
        return [
            [['title', 'text',  'link', 'tag'], 'required'],
            [['text', 'link', 'tag','picture'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['img'],'file','extensions' => 'png, jpg, jpeg, gif'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'img' => Yii::t('app', 'Picture'),
            'link' => Yii::t('app', 'Link'),
            'tag' => Yii::t('app', 'Tag'),
        ];
    }

    //Сохранение изображения
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
                if($img = UploadedFile::getInstance($this, 'img')){
                    @unlink($this->picture);
                    $imgPath = 'uploads/portfolio/' . $this->title . '.' . $img->extension;
                    $img->saveAs($imgPath);
                    $this->picture = $imgPath;
                }
        }
        return true;
    }

}