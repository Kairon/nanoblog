<?php

namespace app\models;

use Yii;


class TagsPosts extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tags_posts';
    }

    public function rules()
    {
        return [
            [['tag_id', 'post_id'], 'required'],
            [['tag_id', 'post_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tag_id' => Yii::t('app', 'Tag ID'),
            'post_id' => Yii::t('app', 'Post ID'),
        ];
    }
}
