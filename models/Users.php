<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;


class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['login', 'pass', 'mail', 'auth_token'], 'required'],
            [['access'], 'integer'],
            ['access' , 'default' , 'value' => '0'],
            [['login', 'mail','pass'], 'string', 'max' => 255],
        ];
    }

    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'pass' => Yii::t('app', 'Pass'),
            'mail' => Yii::t('app', 'Mail'),
            'access' => Yii::t('app', 'Access'),
        ];
    }

    //реализация интерфейса

    public static function findIdentity($id)
      {
          return static::findOne($id);
      }

     public static function findIdentityByAccessToken($token, $type = null)
      {
             return static::findOne(['auth_token' => $token]);
      }

      public function getId()
      {
             return $this->id;
         }

      public function getAuthKey()
      {
             return $this->auth_token;
         }

      public function validateAuthKey($authKey)
      {
             return $this->auth_token === $authKey;
      }

      public function getIsAdmin()
      {
          return $this->access == '1';
      }

    //реализация интерфейса---

    public function register ()
    {

        $this->auth_token = Yii::$app->security->generateRandomString(25);

        $this->pass = Yii::$app->security->generatePasswordHash($this->pass,10);

        return $this->save();
    }

    public function validatePassword($password = false)
    {
        return Yii::$app->security->validatePassword($password,$this->pass);
    }

}
