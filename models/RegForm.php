<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 26.10.16
 * Time: 11:22
 */

namespace app\models;

use Yii;
use yii\base\Model;


class RegForm extends Model
{
    public $username;
    public $email;
    public $pass;
    public $passRepeat;
    public $verifyCode;

    public function rules()
    {
        return [
            [['username','email','pass','passRepeat','verifyCode'],'required'],
            ['username','unique','targetClass'=>Users::className(),'targetAttribute'=>'login'],
            [['username','email','pass'],'string'],

            //[['username','pass'],'max' => 25],
            [['email'],'email'],
            ['pass', 'validatePass'],
            ['verifyCode', 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'pass' => Yii::t('app', 'Password'),
            'passRepeat' => Yii::t('app', 'Pass Repeat'),
            'email' => Yii::t('app', 'Mail'),
            'verifyCode' => Yii::t('app', 'Captcha'),

        ];
    }

    public function validatePass($atribute,$params)
    {
        if($this->pass != $this->passRepeat){
            $this->addError('passRepeat',Yii::t('app','Pass doesn\'t match'));
            return false;
        }
        return true;
    }

    public function reg()
    {
        if(!$this->validate())  return false;

        $user = new Users([
            'login' => $this->username,
            'pass' => $this->pass,
            'mail' => $this->email,
        ]);

        return $user->register();

    }

}