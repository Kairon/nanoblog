<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;



class Posts extends ActiveRecord
{
    //переменные
    public $_tags;
    public $imageFile;

    public static function tableName()
    {
        return 'posts';
    }

    //Правила валидации
    public function rules()
    {
        return [
            [['title', 'full_text', 'description', 'key_words',  'private',  'category_id'], 'required'],
            [['full_text', 'description', 'key_words', 'logo'], 'string'],
            [['date', 'private', 'category_id','user_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['imageFile'],'file',   'extensions' => 'png, jpg, gif'],
            ['date','default','value' => time()],
            ['user_id' ,'default', 'value' => Yii::$app->user->id],
            ['tags', 'safe'],
        ];
    }

    //Поведение ( реализация слагов)
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    //Название записей
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'full_text' => Yii::t('app', 'Full Text'),
            'description' => Yii::t('app', 'Description'),
            'key_words' => Yii::t('app', 'Key Words'),
            'date' => Yii::t('app', 'Date'),
            'private' => Yii::t('app', 'Private'),
            'slug' => Yii::t('app', 'Slug'),
            'category_id' => Yii::t('app', 'Category'),
            'imageFile' => Yii::t('app', 'Logo'),
        ];
    }

    //Вывод тегов $content
    public function showTags(){
        $content = '';
        if (false != ($tags = $this->tags)){
            foreach ($tags as $tag){
                $content .= Html::a($tag->name, ['/site/tag', 'tag' => $tag->name]);
                //$content .= ' ' ;
            }
        }
        return $content;
    }

    //Вывод категории
    /*public function showCat()
    {
        return $this->category->name;
    }*/

    //связь post_id / id (hasMany)
    public function getTagsPosts()
    {
        return $this->hasMany(TagsPosts::className(),['post_id' => 'id']);
    }

    //связь id / tags via TagsPosts (hasMany)
    public function getTags()
    {
        if(!empty($this->_tags)){
            return $this->_tags;
        }
        return $this->hasMany(Tags::className(),['id' => 'tag_id'])->via('tagsPosts');
    }

    //связь id / category_id (hasOne)
    public function getCategory()
    {
        return $this->hasOne(Categories::className(),['id' => 'category_id']);
    }

    // связь id / user_id (hasOne)
    public function getUser()
    {
        return $this->hasOne(Users::className(),['id' => 'user_id']);
    }

    //утановка значения тега
    public function setTags($tags)
    {
        $this->_tags = $tags;
    }

    //обновление тегов записи
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if(!empty($this->_tags)){
            TagsPosts::deleteAll(['post_id' => $this->id]);
            foreach($this->_tags as $tag){
                $model = new TagsPosts([
                    'post_id' => $this->id,
                    'tag_id' => $tag,
                ]);
                $model->save();
            }
        }
        return true;
    }

    public function getShort($length = 255){
        return mb_strlen($this->full_text, 'utf-8') > 255 ? mb_substr($this->full_text, 0, 255, 'utf-8') . '...' : $this->full_text;
    }

    public function getLogo()
    {
        return $this->logo ? '/'. $this->logo : false ;
    }

}
