<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

class Tags extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tags';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 25],
            [['name'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }
    public static function getAll()
    {
        $content = '';
        foreach(self::find()->all() as $tag){
            $content .= Html::a($tag->name, ['/site/tag', 'tag' => $tag->name]);
        }
        return $content;
    }
}
