<?php

namespace app\models;

use Yii;
use yii\helpers\Html;


class Categories extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'categories';
    }

    //Правила валидации
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [[ 'parent_id'], 'integer'],
            ['parent_id','default','value' => '0'],
            [['description', 'key_words'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'key_words' => Yii::t('app', 'Key Words'),
            'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }

  /*  public function getItems(){
        $items = [];
        foreach(self::find()->all() as $category){
            $items[] = ['label' => $category->name, 'url' => ['/category', 'id' => $category->id]];
        }
        return $items;
    }*/

    public function getCount(){
        $count = Posts::find()->where(['category_id' =>$this->id ,'private' => '0'])->count();
        return $count;
    }

}
