<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Posts;

/**
 * PostSearch represents the model behind the search form about `app\models\Posts`.
 */
class PostSearch extends Posts
{
    public $tag;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date', 'private', 'category_id','user_id'], 'integer'],
            [['title', 'full_text', 'description', 'key_words', 'slug','tag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = 'PostSearch')
    {
        if($formName !='PostSearch'){
            $query = Posts::find()->where(['private'=>'0']);
        }else {
            $query = Posts::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['tags','category']);

        // grid filtering conditions
        $query->andFilterWhere([
            'posts.id' => $this->id,
            'date' => $this->date,
            'private' => $this->private,
            'category_id' => $this->category_id,
            'tags.name' => $this->tag,

            'user_id' => $this->user_id,
        ]);


        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'full_text', $this->full_text])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'key_words', $this->key_words])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        //фильт юзеров

        //фильт юзеров---

        return $dataProvider;
    }
}
