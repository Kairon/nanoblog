-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 15 2016 г., 17:38
-- Версия сервера: 10.0.27-MariaDB-0ubuntu0.16.04.1
-- Версия PHP: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `nanoblog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `key_words` text,
  `parent_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `key_words`, `parent_id`) VALUES
(8, 'Общее', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 'All,do,want', 0),
(9, 'Мои Заметки', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 0),
(10, 'Полезное', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 0),
(11, 'twst', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet. ', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `picture` text NOT NULL,
  `link` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `portfolios`
--

INSERT INTO `portfolios` (`id`, `title`, `text`, `picture`, `link`, `tag`) VALUES
(6, 'Мой первый проект', 'Описание к моему проекту', 'uploads/portfolio/Мой первый проект.jpg', 'google.com', 'Falker'),
(7, 'Project Nervana', 'Text album into de peria Cresau tu si unde rton Text album into de peria Cresau tu si unde rton Text album into de peria Cresau tu si unde rton', 'uploads/portfolio/Project Nervana.gif', '/', 'Falker'),
(8, 'Дебютный проект наноблог', 'Здесь находиться описание моего дебютного самого любимого проекта под название "Наноблог"\r\nЗдесь находиться описание моего дебютного самого любимого проекта под название "Наноблог"\r\nЗдесь находиться описание моего дебютного самого любимого проекта под название "Наноблог"\r\n', 'uploads/portfolio/Дебютный проект наноблог.jpg', '/', 'Лучшие'),
(9, 'уеифыуеирф', 'уцифукифуекиф', 'uploads/portfolio/уеифыуеирф.jpg', 'фукифук', 'ифукифуки'),
(10, 'фукпмфукп', 'уйкепийукп', 'uploads/portfolio/фукпмфукп.jpg', 'фукиуки', 'фукифуки');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `full_text` text NOT NULL,
  `description` text NOT NULL,
  `key_words` text NOT NULL,
  `date` int(11) NOT NULL,
  `private` tinyint(4) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `logo`, `full_text`, `description`, `key_words`, `date`, `private`, `slug`, `category_id`, `user_id`) VALUES
(8, 'Excepteur Sint ecat Cupidatat.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et eros justo. Integer ornare tincidunt quam pharetra posuere. Maecenas fringilla massa nec sem venenatis aliquam. Quisque at nibh quis magna ultrices fermentum vitae a diam. Fusce vitae laoreet dui. Donec porta non sapien at iaculis. Cras venenatis nisl at sollicitudin aliquet.\r\n\r\nVestibulum bibendum enim quis porta lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In accumsan ante augue, ac finibus leo tristique sagittis. Duis et tellus blandit, fermentum metus nec, lobortis ligula. Mauris cursus sit amet magna sed rhoncus. Phasellus aliquam lacus porttitor ligula semper feugiat. Vestibulum sed est augue. Maecenas eget urna nec enim malesuada lobortis. Maecenas ultricies justo eu lorem pellentesque faucibus. Aliquam erat volutpat. Aliquam eget tellus leo. Suspendisse a eros id nisl imperdiet tincidunt. Aliquam a suscipit nunc. Vestibulum viverra nisi risus, eu placerat sapien consectetur sed.\r\n\r\nInteger lacinia efficitur nunc, a maximus lectus semper quis. Aenean porta velit vitae commodo volutpat. Aliquam in egestas odio. Duis ut lorem quam. Nullam non enim laoreet, pulvinar nulla varius, sagittis nulla. Donec malesuada volutpat lorem, quis sollicitudin nisi finibus ac. Nunc tincidunt a dolor vel porttitor. Morbi ultricies metus elit, vel tristique neque lobortis a. Nulla malesuada odio eget pretium ultrices. Pellentesque venenatis urna at tortor venenatis, sed bibendum velit pharetra. Nulla auctor est ut ligula rhoncus, vitae aliquet augue bibendum.\r\n\r\nAliquam erat volutpat. Duis malesuada tortor felis, eget suscipit orci placerat vel. Aliquam ac justo tortor. Aliquam erat volutpat. In tristique nunc eu venenatis egestas. Vivamus ac eleifend est. Ut feugiat consectetur mauris sed condimentum. Phasellus maximus eu justo nec convallis. In sit amet aliquet diam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas molestie ipsum orci, in vulputate tortor dapibus nec. Fusce et finibus dui, quis consequat ipsum. Aliquam at tincidunt turpis. Praesent semper rutrum leo, et euismod enim feugiat et.\r\n\r\nNullam bibendum elit vitae condimentum placerat. Sed vestibulum augue sed maximus finibus. Nullam vitae mauris tincidunt, laoreet est et, tristique sapien. Mauris vehicula leo erat, in malesuada magna maximus vel. Suspendisse efficitur urna magna, ut ultricies velit blandit sit amet. Praesent eros purus, euismod ac mattis ut, aliquet et erat. Etiam auctor luctus sem ut dignissim. Phasellus eget sem id quam tempor finibus sit amet quis mi. ', 'aewrg', 'qaertgqwer', 1478102286, 0, 'excepteur-sint-ecat-cupidatat', 9, 5),
(9, 'ea3y3q4', 'uploads/ea3y3q4.gif', 'tgq34tgq34tg', 'q3', 'tg4q34tgq34', 1478248007, 0, 'ea3y3q4', 8, 5),
(10, 'Тестовая запись', 'uploads/Тестовая запись.jpg', ' фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п', ' фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п', ' фшукапшфупркапуйшкпр фшукапшфупркапуйшкпр ыунп цукп цуп цуп цук пцу кп цук пцу п', 1478250046, 0, 'testovaa-zapis', 8, 5),
(11, 'Тестовая запись', 'uploads/Тестовая запись.jpg', 'eataerger eataerger eataerger eataerger eataerger eataerger eataerger \r\neataerger eataerger eataerger eataerger eataerger \r\neataerger eataerger \r\neataerger eataerger eataerger eataerger eataerger eataerger eataerger \r\neataerger eataerger eataerger v', 'eataerger eataerger ', 'eataerger eataerger ', 1478256667, 0, 'testovaa-zapis', 9, 3),
(12, 'ergwerg', '', 'wergqewr', 'gqergqerg', 'geqrg', 1478256950, 0, 'ergwerg', 8, 3),
(13, 'ergerg', 'uploads/ergerg.gif', 'gaw3rg4rg', 'aergaer', 'gerg', 1478274881, 0, 'ergerg', 8, 5),
(14, 'esrg', 'uploads/esrg.jpg', 'erg', 'eargaerg', 'aergaerg', 1478275117, 0, 'esrg', 11, 5),
(15, 'название1', '', '<h1>Это будет тестовая запись</h1>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<p>Эта запись использует визуальный редактор</p>\r\n\r\n<p>ниже приведен код его подключения:</p>\r\n\r\n<div style="background:#eeeeee; border:1px solid #cccccc; padding:5px 10px"><code>use dosamigos\\ckeditor\\CKEditor;</code></div>\r\n\r\n<p><var>&lt;?= $form-&gt;field($model, &#39;text&#39;)-&gt;widget(CKEditor::className(), [<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#39;options&#39; =&gt; [&#39;rows&#39; =&gt; 6],<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#39;preset&#39; =&gt; &#39;basic&#39;<br />\r\n&nbsp;&nbsp;&nbsp; ]) ?&gt;</var></p>\r\n\r\n<p>Вот так вот !</p>\r\n', 'цупацуп', 'цупцуп', 1479123070, 0, 'nazvanie1', 10, 5),
(16, 'cкрытый пост', '', '<p>wefwe</p>\r\n', 'wef', 'wefwef', 1479215257, 1, 'ckrytyj-post', 11, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'tag'),
(2, 'tag1'),
(3, 'tag3'),
(4, 'tag4'),
(5, 'tag5');

-- --------------------------------------------------------

--
-- Структура таблицы `tags_posts`
--

CREATE TABLE `tags_posts` (
  `tag_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `tags_posts`
--

INSERT INTO `tags_posts` (`tag_id`, `post_id`) VALUES
(1, 5),
(2, 5),
(1, 6),
(2, 6),
(3, 6),
(4, 6),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(1, 8),
(2, 8),
(1, 9),
(5, 10),
(2, 11),
(3, 11),
(4, 11),
(3, 12),
(5, 14),
(2, 15),
(2, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `access` tinyint(4) NOT NULL,
  `auth_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `pass`, `mail`, `access`, `auth_token`) VALUES
(3, 'test', '$2y$10$uKy9EOsxZsV1OJfE6oQgPuVNtJZy9W.BdK2Op5eGHELefkdsjadDC', 'sdf@mail.ru', 0, 'vIN03knqA6JyBzegQ08-thd00'),
(4, 'test2', '$2y$10$oVg3S4hAk0Vzh81Hrbezju.dHo7yAzSbfJsHeVW4PEVZFqX4RU8qK', 'sdf@mail.ru', 0, 'bk-VI2F2wwr2b0zM1Adhs4lVd'),
(5, 'admin', '$2y$10$xOViUnbvKkR4Z83bvTNi2OM9.mssf.kPb2Ob/xDCOQD8NQOw5xUrG', 'admin@mail.ru', 1, 'QZ-nKa3ibunKGf1y1gG9Lu2_q'),
(7, 'awergaer', '$2y$10$CTf7WyXtcbJTtg7IFe94BOvI2HwVn9zvd1psPC9iL5xk2BgrFdblS', 'aergaerg@mail.ru', 0, 'MOLPiJRxyambzq1ZQGa1KuzeN'),
(8, 'aergaewrg', '$2y$10$dd8v8pLjyBh5IFvXZC4rKu.BSqxEEPgFe1R.YT9drtSRbqUj0TIN.', 'aergaerg@mail.ru', 0, 'KuDU_UzW3p6zCbeP3y-1fa_1i');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`(191));

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `private` (`private`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `tags_posts`
--
ALTER TABLE `tags_posts`
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_2` (`login`),
  ADD KEY `login` (`login`),
  ADD KEY `mail` (`mail`(191)),
  ADD KEY `auth_token` (`auth_token`(191));

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
